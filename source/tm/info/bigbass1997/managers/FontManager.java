package tm.info.bigbass1997.managers;

import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

public class FontManager {

	/*
	 * Survivor is a 2D Role-Playing Game.
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */

	public static AngelCodeFont F12, F16, F22;
	
	public static void initAllFonts() {
		F12 = getFontstructionFontACF12();
		F16 = getFontstructionFontACF16();
		F22 = getFontstructionFontACF22();
	}

	private static AngelCodeFont getFontstructionFontACF12() {
		try {
			AngelCodeFont font = new AngelCodeFont("resources/fonts/the_first_fontstruction/the_first_fontstruction12.fnt", new Image("resources/fonts/the_first_fontstruction/the_first_fontstruction12.png"));
			return font;
		} catch (SlickException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static AngelCodeFont getFontstructionFontACF16() {
		try {
			AngelCodeFont font = new AngelCodeFont("resources/fonts/the_first_fontstruction/the_first_fontstruction16.fnt", new Image("resources/fonts/the_first_fontstruction/the_first_fontstruction16.png"));
			return font;
		} catch (SlickException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static AngelCodeFont getFontstructionFontACF22() {
		try {
			AngelCodeFont font = new AngelCodeFont("resources/fonts/the_first_fontstruction/the_first_fontstruction22.fnt", new Image("resources/fonts/the_first_fontstruction/the_first_fontstruction22.png"));
			return font;
		} catch (SlickException e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static UnicodeFont getFontstructionFont(int size) {
		try {
			UnicodeFont font = new UnicodeFont("resources/fonts/the-first-fontstruction.ttf", size, false, false);
			font.addGlyphs(0, 9999);
			font.getEffects().add(new ColorEffect());
			font.loadGlyphs();
			return font;
		} catch (SlickException e) {
			e.printStackTrace();
		}
		return null;
	}
}
