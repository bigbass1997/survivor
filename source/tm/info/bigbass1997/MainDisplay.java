package tm.info.bigbass1997;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import tm.info.bigbass1997.managers.HTMLFormatter;
import tm.info.bigbass1997.states.GamePlayState;
import tm.info.bigbass1997.states.MainMenuState;

public class MainDisplay extends StateBasedGame {
	
	/*
	 * Survivor is a 2D Role-Playing Game.
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */

	// SPECIAL VARIABLES\\
	public static AppGameContainer appGC;
	public static final int SWIDTH = 1000, SHEIGHT = 700;
	public static final String title = "Survivor ", version = "0.0.0 (Alpha)";
	public static boolean debug = true;

	public static Graphics g;
	public static GameContainer mgc;

	public static Logger logger = Logger.getLogger("Logger");
	public static FileHandler fh;

	// INTS\\
	public static int mouseX = 0, mouseY = 0;

	public static final int MAINMENUSTATE = 0;
	public static final int GAMEPLAYSTATE = 1;

	// STRINGS\\

	// IMAGES\\

	// BOOLEANS\\

	// CONSTRUCTOR\\
	public MainDisplay() {
		super(title + version);
	}

	public static void main(String[] args) {
		try {
			fh = new FileHandler("log.html");
			fh.setFormatter(new HTMLFormatter());
			logger.addHandler(fh);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			//Renderer.setRenderer(Renderer.VERTEX_ARRAY_RENDERER); // EXPERIMENTAL FUNCTION >> http://slick.cokeandcode.com/wiki/doku.php?id=performance_memory_tips
			appGC = new AppGameContainer(new MainDisplay());
			appGC.setTitle(title);
			appGC.setDisplayMode(SWIDTH, SHEIGHT, false);
			appGC.setShowFPS(false);
			appGC.setVerbose(false);
			appGC.setTargetFrameRate(60);
			appGC.setAlwaysRender(true);
			Debug.logInfo("Main Method Loaded!");
			appGC.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		this.addState(new MainMenuState(MAINMENUSTATE));
		this.addState(new GamePlayState(GAMEPLAYSTATE));
	}

	public void exit(GameContainer gc) {
		gc.exit();
	}
}
