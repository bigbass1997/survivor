package tm.info.bigbass1997.world;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.tiled.TiledMap;

import tm.info.bigbass1997.MainDisplay;
import tm.info.bigbass1997.managers.ShapeManager;
import tm.info.bigbass1997.objects.Player;

public class World {
	
	private Player player;
	
	private TiledMap map;
	private float mapX, mapY, mapWidth, mapHeight;
	
	public World(){
		
	}

	public void init(GameContainer gc, StateBasedGame sbg) {
		player = new Player((MainDisplay.SWIDTH / 2) - 25.0f, (MainDisplay.SHEIGHT / 2) - 25.0f);
		player.init(gc, sbg);
		
		try {
			map = new TiledMap("resources/images/tiles/world1.tmx", true);
		} catch (SlickException e) {
			e.printStackTrace();
		}
		mapX = 0;
		mapY = 0;
		mapWidth = map.getWidth();
		mapHeight = map.getHeight();
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		map.render((int) mapX, (int) mapY, 0);
		
		//System.out.println(map.getTileId(1, 1, 0)); //GET TILE ID OF POS 1, 1 (x, y)
		
		g.draw(ShapeManager.Rect(g, 100, 100, gc.getWidth() - 200, gc.getHeight() - 200, 0xFF000000));
		
		player.render(gc, sbg, g);
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		Input input = gc.getInput();
		
		player.update(gc, sbg, delta);
		
		if(player.getX() <= 100 && input.isKeyDown(Input.KEY_LEFT) && mapX <= 0) mapX += (delta * 0.1f);
		if(player.getX() >= MainDisplay.SWIDTH - 200 && input.isKeyDown(Input.KEY_RIGHT)) mapX -= (delta * 0.1f);
		
		if(player.getY() <= 100 && input.isKeyDown(Input.KEY_UP) && mapY <= 0) mapY += (delta * 0.1f);
		if(player.getY() >= MainDisplay.SHEIGHT - 200 && input.isKeyDown(Input.KEY_DOWN)) mapY -= (delta * 0.1f);
	}
}
