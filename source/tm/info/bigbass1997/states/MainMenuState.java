package tm.info.bigbass1997.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import tm.info.bigbass1997.Debug;
import tm.info.bigbass1997.MainDisplay;
import tm.info.bigbass1997.managers.FontManager;
import tm.info.bigbass1997.managers.ImageManager;
import tm.info.bigbass1997.managers.ShapeManager;
import tm.info.bigbass1997.objects.CustomButton;

public class MainMenuState extends BasicGameState {
	
	/*
	 * Survivor is a 2D Role-Playing Game.
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */
	
	private static int SWIDTH, SHEIGHT;
	public static int pointerx = -1000, pointery = -1000;

	public static CustomButton playButton;

	public static int FPS = 0;

	public static int mouseX = 0, mouseY = 0;

	@SuppressWarnings("unused")
	private static String FPSs;

	int stateID = -1;

	public MainMenuState(int stateID) {
		this.stateID = stateID;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		SWIDTH = MainDisplay.SWIDTH;
		SHEIGHT = MainDisplay.SHEIGHT;

		FontManager.initAllFonts();

		// gc.setMinimumLogicUpdateInterval(80);
		// gc.setMaximumLogicUpdateInterval(160);
		
		FPS = MainDisplay.appGC.getFPS();
		FPSs = "FPS: " + FPS;

		ImageManager.loadImages();
		
		playButton = new CustomButton(350, 70, 300, 50, 100, 0, "Play!", 0xFF444444, 0xFFFFFFFF, FontManager.F22);

		Debug.logInfo("MainMenu Initialization Method Loaded!");
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		g.fill(ShapeManager.Rect(g, 0, 0, SWIDTH, SHEIGHT, 0xFF131356));

		playButton.render(gc, sbg, g);

		//g.drawImage(ImageManager.getImage(2), MainMenuState.mouseX, MainMenuState.pointery);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		playButton.update(gc, sbg, delta);
	}

	@Override
	public int getID() {
		return stateID;
	}
}
