package tm.info.bigbass1997;

public class Debug {
	
	/*
	 * Survivor is a 2D Role-Playing Game.
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */
	
	public static boolean debug = true;
	
	public static void logInfo(String s){
		if(debug) MainDisplay.logger.info(s);
	}
	
	public static void logWarning(String s){
		if(debug) MainDisplay.logger.warning(s);
	}
	
	public static void logSevere(String s){
		if(debug) MainDisplay.logger.severe(s);
	}
}
