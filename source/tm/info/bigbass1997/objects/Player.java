package tm.info.bigbass1997.objects;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.state.StateBasedGame;

import tm.info.bigbass1997.MainDisplay;
import tm.info.bigbass1997.managers.ShapeManager;

public class Player {
	
	private float x, y, width, height;
	
	private Polygon hitbox;
	
	private SpriteSheet sheet;

	private Animation currentAnim, walkLeft, walkRight, walkUp, walkDown;
	private Image[] walkingLeft, walkingRight, walkingUp, walkingDown;
	private Image defaultImage;
	
	private boolean movingRight = false, movingLeft = false, movingUp = false, movingDown = false;
	
	public Player(float x, float y){
		this.x = x;
		this.y = y;
	}

	public void init(GameContainer gc, StateBasedGame sbg) {
		try {
			sheet = new SpriteSheet("resources/images/player/sheet.png", 64, 64);
		} catch (SlickException e) {
			e.printStackTrace();
		}
		defaultImage = sheet.getSubImage(0, 0);
		
		width = defaultImage.getWidth();
		height = defaultImage.getHeight();
		
		hitbox = ShapeManager.Poly((int) x, (int) y, defaultImage.getWidth(), defaultImage.getHeight());
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.drawImage(defaultImage, x, y);
		g.draw(ShapeManager.Rect(g, hitbox, 0xFFFF0000));
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) {
		Input input = gc.getInput();

		if(input.isKeyDown(Input.KEY_RIGHT)){movingRight = true;}else{movingRight = false;}
		if(input.isKeyDown(Input.KEY_LEFT)){movingLeft = true;}else{movingLeft = false;}
		if(input.isKeyDown(Input.KEY_UP)){movingUp = true;}else{movingUp = false;}
		if(input.isKeyDown(Input.KEY_DOWN)){movingDown = true;}else{movingDown = false;}
		
		float moveSpeed = (delta * 0.1f);
		
		if(movingRight) x += moveSpeed;
		if(movingLeft) x -= moveSpeed;
		if(movingUp) y -= moveSpeed;
		if(movingDown) y += moveSpeed;
		
		if(x <= 50.0f){
			x += moveSpeed;
			movingLeft = false;
		}
		if(x + width >= (float) MainDisplay.SWIDTH - 50.0f){
			x -= moveSpeed;
			movingRight = false;
		}
		if(y <= 50.0f){
			y += moveSpeed;
			movingUp = false;
		}
		if(y + height >= (float) MainDisplay.SHEIGHT - 50.0f){
			y -= moveSpeed;
			movingDown = false;
		}

		hitbox = ShapeManager.Poly((int) x, (int) y, defaultImage.getWidth(), defaultImage.getHeight());
	}
	
	public float getX(){return x;}
	public void setX(float x){this.x = x;}
	public void setX(int x){this.x = (float) x;}
	public float getY(){return y;}
	public void setY(float y){this.y = y;}
	public void setY(int y){this.y = (float) y;}
	
	public Polygon getHitBox(){return hitbox;}

	public boolean isMovingRight(){return movingRight;}
	public boolean isMovingLeft(){return movingLeft;}
	public boolean isMovingUp(){return movingUp;}
	public boolean isMovingDown(){return movingDown;}
}
