package tm.info.bigbass1997.objects;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import tm.info.bigbass1997.MainDisplay;
import tm.info.bigbass1997.objects.inventory.Inventory;

public class GUI {
	
	/*
	 * Survivor is a 2D Role-Playing Game.
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */
	
	private int x = 0, y = 0, width, height;
	
	private Inventory playerInv;
	private boolean invOpen = false;
	
	public GUI(){
		this.width = MainDisplay.SWIDTH;
		this.height = MainDisplay.SHEIGHT;
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		playerInv = new Inventory(690, 50, 5, 5, 48);
		playerInv.init(gc, sbg);
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		if(invOpen) playerInv.render(gc, sbg, g);
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException{
		Input input = gc.getInput();
		
		if(input.isKeyPressed(Input.KEY_I)){
			if(invOpen) invOpen = false; else
			if(!invOpen) invOpen = true;
		}
		
		if(invOpen) playerInv.update(gc, sbg, delta);
	}
}
