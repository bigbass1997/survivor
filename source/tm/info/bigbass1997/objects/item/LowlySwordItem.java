package tm.info.bigbass1997.objects.item;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import tm.info.bigbass1997.objects.Stat;

public class LowlySwordItem extends Item{
	
	/*
	 * Survivor is a 2D Role-Playing Game.
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */
	
	private final int stackSize = 64;
	
	private final static int strength = 5, agility = 5;
	
	public LowlySwordItem() throws SlickException {
		super(new Image("resources/images/items/lowly_sword_item.png"), "== Lowly Sword ==", new Stat[]{
			new Stat("Strength", strength),
			new Stat("Agility", agility)
		});
	}
}
