package tm.info.bigbass1997.objects.inventory;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import tm.info.bigbass1997.managers.ShapeManager;
import tm.info.bigbass1997.objects.item.Item;
import tm.info.bigbass1997.states.GamePlayState;

public class Slot {
	
	/*
	 * Survivor is a 2D Role-Playing Game.
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */
	
	private int x, y, width, height;
	
	private boolean highlight, drawToolTip;
	
	private Item item;
	
	public Slot(int x, int y, int width, int height){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		highlight = false;
		drawToolTip = false;
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		g.fill(ShapeManager.Rect(g, x, y, width, height, 0xFFCCCCCC));
		g.draw(ShapeManager.Rect(g, x, y, width, height, 0xFF000000));
		
		//if(item != null)System.out.println("Item = " + item.getImage());
		
		if(item != null && item.getImage() != null) g.drawImage(item.getImage(), x, y);
		//System.out.println(item != null);
		
		if(highlight){
			g.fill(ShapeManager.Rect(g, x, y, width, height, 0xFF000000));
		}
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		Input input = gc.getInput();
		int mx = input.getMouseX();
		int my = input.getMouseY();
		
		if(this.item != null){
			if(mx >= x && mx <= x + width && my >= y && my <= y + height){
				drawToolTip = true;
			}else{
				drawToolTip = false;
			}
			item.getToolTip().update(gc, sbg, delta);
		}
	}
	
	public void transferItem(){
		if(item != null && GamePlayState.mouseItem == null){
			GamePlayState.mouseItem = item;
			item = null;
		}else if(item == null && GamePlayState.mouseItem != null){
			item = GamePlayState.mouseItem;
			GamePlayState.mouseItem = null;
		}
	}
	
	public void setItem(Item item){this.item = item;}
	public Item getItem(){return this.item;}
	
	public int getX(){return x;}
	public int getY(){return y;}
	public int getWidth(){return width;}
	public int getHeight(){return height;}
	
	public boolean getHighlight(){return highlight;}
	public void setHighlight(boolean hl){highlight = hl;}

	public boolean getDrawToolTip(){return drawToolTip;}
}
