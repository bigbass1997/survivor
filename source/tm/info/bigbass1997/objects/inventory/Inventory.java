package tm.info.bigbass1997.objects.inventory;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import tm.info.bigbass1997.managers.ShapeManager;
import tm.info.bigbass1997.objects.item.*;

public class Inventory {
	
	/*
	 * Survivor is a 2D Role-Playing Game.
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */
	
	//TODO Make items stackable
	
	private int x, y, width, height;
	private int rows, columns;
	private int itemSize;
	
	private int buffer = 3;
	
	private Slot[][] slots;
	
	public Inventory(int x, int y, int rows, int columns, int itemSize){
		this.x = x;
		this.y = y;
		this.rows = rows;
		this.columns = columns;
		this.itemSize = itemSize;
		
		width = (this.columns * (itemSize + buffer)) + buffer;
		height = (this.rows * (itemSize + buffer)) + buffer;
		
		this.slots = new Slot[rows][columns];
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		for(int r = 0; r < this.rows; r++){
			for(int c = 0; c < this.columns; c++){
				slots[r][c] = new Slot((x + buffer) + ((itemSize + buffer) * c), (y + buffer) + ((itemSize + buffer) * r), itemSize, itemSize);
			}
		}
		slots[0][0].setItem(new LowlySwordItem());
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		g.fill(ShapeManager.Rect(g, x, y, width, height, 0xFF603311));
		
		g.draw(ShapeManager.Rect(g, x, y, width, height, 0xFF603311));
		g.draw(ShapeManager.Rect(g, x + 1, y + 1, width - 2, height - 2, 0xFF603311));
		g.draw(ShapeManager.Rect(g, x + 2, y + 2, width - 4, height - 4, 0xFF603311));

		for(int r = 0; r < this.rows; r++){
			for(int c = 0; c < this.columns; c++){
				slots[r][c].render(gc, sbg, g);
				
				//System.out.println("R: " + (r + 1) + " | C: " + (c + 1));
			}
		}

		for(int r = 0; r < this.rows; r++){
			for(int c = 0; c < this.columns; c++){
				if(slots[r][c].getDrawToolTip() && slots[r][c].getItem() != null){
					slots[r][c].getItem().getToolTip().render(gc, sbg, g);
				}
			}
		}
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		Input input = gc.getInput();
		int mx = input.getMouseX();
		int my = input.getMouseY();
		boolean clicked = input.isMousePressed(0);
		
		for(int r = 0; r < this.rows; r++){
			for(int c = 0; c < this.columns; c++){
				//System.out.println(slots[r][c].getItem());
				
				slots[r][c].update(gc, sbg, delta);
				
				// ITEM NAME EXAMPLE
				//if(slots[r][c].getItem() != null && slots[r][c].getItem().getName() == "NULL_ITEM") {System.out.println("R: " + (r + 1) + " | C: " + (c + 1));}
				
				if(clicked &&
						mx >= slots[r][c].getX() && mx <= (slots[r][c].getX() + slots[r][c].getWidth()) &&
						my >= slots[r][c].getY() && my <= (slots[r][c].getY() + slots[r][c].getHeight())){
					slots[r][c].transferItem();
				}
			}
		}
		
		width = (this.columns * (itemSize + buffer)) + buffer;
		height = (this.rows * (itemSize + buffer)) + buffer;
	}
	
	public void setBuffer(int buf){buffer = buf;}
}
