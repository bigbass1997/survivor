package tm.info.bigbass1997.objects;

import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import tm.info.bigbass1997.managers.ShapeManager;
import tm.info.bigbass1997.managers.Util;

public class CustomButton {

	/*
	 * Survivor is a 2D Role-Playing Game.
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */

	private int mx, my;
	private int x, y, width, height, xOffSet, yOffSet, argb, sArgb;
	private AngelCodeFont font;
	private String text;

	private static boolean highlight = false;

	public CustomButton() {

	}

	public CustomButton(int bx, int by, int bwidth, int bheight, int xOffSet, int yOffSet, String text, int argb, int sArgb, AngelCodeFont font) {
		this.x = bx;
		this.y = by;
		this.width = bwidth;
		this.height = bheight;
		this.xOffSet = xOffSet;
		this.yOffSet = yOffSet;
		this.text = text;
		this.argb = argb;
		this.sArgb = sArgb;
		this.font = font;
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		g.fill(ShapeManager.Rect(g, x, y, width, height, argb));
		ShapeManager.String(g, text, x + xOffSet, y + yOffSet, sArgb, font);

		g.draw(ShapeManager.Rect(g, x, y, width, height, 0xFF000000));
		g.draw(ShapeManager.Rect(g, x + 1, y + 1, width - 2, height - 2, 0xFF000000));
		g.draw(ShapeManager.Rect(g, x + 2, y + 2, width - 4, height - 4, 0xFF000000));
		g.draw(ShapeManager.Rect(g, x + 3, y + 3, width - 6, height - 6, 0xFF000000));
		
		if(highlight) g.fill(ShapeManager.Rect(g, x, y, width, height, 0x47FFFFFF));
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		Input input = gc.getInput();
		mx = input.getMouseX();
		my = input.getMouseY();

		if(mx >= x && mx <= (x + width) && my >= y && my <= (y + height)){
			highlight = true;
		} else {
			highlight = false;
		}
		
		if(Util.checkBounds(mx, my, x, y, width, height) && input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON)) sbg.enterState(1);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
}
