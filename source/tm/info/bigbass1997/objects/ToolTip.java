package tm.info.bigbass1997.objects;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import tm.info.bigbass1997.MainDisplay;
import tm.info.bigbass1997.managers.FontManager;
import tm.info.bigbass1997.managers.ShapeManager;

public class ToolTip {
	
	/*
	 * Survivor is a 2D Role-Playing Game.
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */
	
	private int x, y, width, height;
	private String itemName;
	
	private Stat[] stats;
	
	private int sHeight;
	
	public ToolTip(String itemName){
		this.itemName = itemName;
		
		x = 0;
		y = 0;
		width = 160;
		height = 80;
		
		sHeight = FontManager.F16.getHeight("AaBbCcDdEeFfGgHh");
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		g.fill(ShapeManager.Rect(g, x, y, width, height, 0xFF001156));
		
		ShapeManager.String(g, itemName, (x + (width / 2)) - (FontManager.F16.getWidth(itemName) / 2), y + 4, 0xFFFFFFFF, FontManager.F16);
		
		for(int i = 1; i <= stats.length; i++){
			ShapeManager.String(g, stats[i - 1].getName() + ": +" + stats[i - 1].getValue(), x + 8, y + ((sHeight + 1) * i) + 3, 0xFF3DFF4A, FontManager.F16);
			
			if(((i + 1) * sHeight) > height - 8) height = ((i + 2) * sHeight);
		}
		
		g.draw(ShapeManager.Rect(g, x, y, width, height, 0xFF8860FF));
		g.draw(ShapeManager.Rect(g, x + 1, y + 1, width - 2, height - 2, 0xFF8860FF));
		g.draw(ShapeManager.Rect(g, x + 2, y + 2, width - 4, height - 4, 0xFF8860FF));
		g.draw(ShapeManager.Rect(g, x + 3, y + 3, width - 6, height - 6, 0xFF8860FF));
		g.draw(ShapeManager.Rect(g, x + 4, y + 4, width - 8, height - 8, 0xFF8860FF));
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		Input input = gc.getInput();
		int mx = input.getMouseX();
		int my = input.getMouseY();
		
		if(mx + (10 + width) >= MainDisplay.SWIDTH){
			x = mx - width;
		}else{
			x = mx + 10;
		}
		
		if(my - (5 + height) <= 0){
			y = my + 20;
		}else{
			y = my - (5 + height);
		}
	}
	
	public void setStats(Stat[] stats){
		this.stats = stats;
	}
}
