package tm.info.bigbass1997.objects;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.state.StateBasedGame;

import tm.info.bigbass1997.managers.ShapeManager;

public class Object {
	
	/*
	 * Survivor is a 2D Role-Playing Game.
	 * Copyright (C) 2013 Bigbass1997
	 * 
	 * This program is free software: you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License 
	 * as published by the Free Software Foundation, either version
	 * 3 of the License, or (at your option) any later version.
	 * 
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	 * GNU General Public License for more details.
	 * 
	 * You should have received a copy of the GNU General Public
	 * License along with this program.
	 * If not, see <http://www.gnu.org/licenses/>
	 */
	
	// ANIMATION REFERANCE http://bit.ly/179YYFF
	//new Object(50, 50, 16, 16, new Image[]{new Image("resources/images/anim1.png"), new Image("resources/images/anim2.png")}, new int[]{200, 200});
	
	private int x, y, width, height;
	
	private int[] animDurations;
	private Image[] animImages;
	private Animation anim;
	
	private Color color;
	
	private Polygon hitBox;
	
	public Object(int x, int y, int width, int height, Image[] animImages, int[] animDurations){ // Object Initialization
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		this.animImages = animImages;
		this.animDurations = animDurations;
	}

	public Object(int x, int y, int width, int height, Image[] animImages, int[] animDurations, Color color){ // Object Initialization
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		this.animImages = animImages;
		this.animDurations = animDurations;
		
		this.color = color;
	}
	
	public Object(int x, int y, int width, int height, Color color){ // Object Initialization
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		this.color = color;
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) {
		if(animImages != null && animDurations != null) anim = new Animation(animImages, animDurations, false);
		
		hitBox = ShapeManager.Poly(x, y, width, height);
		
		if(color == null) color = new Color(0xFF000000);
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException { // Object Render
		g.setColor(new Color(color));
		g.fill(hitBox);
		
		if(anim != null) anim.draw(x, y);
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException { // Object Update
		hitBox = ShapeManager.Poly(x, y, width, height);
		
		if(anim != null) anim.update(delta);
	}
	
	public int getX(){return x;}
	public void setX(int x){this.x = x;}
	
	public int getY(){return y;}
	public void setY(int y){this.y = y;}
	
	public int getWidth(){return width;}
	public void setWidth(int width){this.width = width;}
	
	public int getHeight(){return height;}
	public void setHeight(int height){this.height = height;}
	
	public Polygon getHitBox(){return hitBox;}
	
	public void setColor(Color argb){color = argb;}
}
